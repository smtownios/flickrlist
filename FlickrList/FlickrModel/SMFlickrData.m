//
//  SMFlickrData.m
//
//  Created by   on 2018. 11. 28.
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "SMFlickrData.h"
#import "SMPhotos.h"


NSString *const kSMFlickrDataStat = @"stat";
NSString *const kSMFlickrDataPhotos = @"photos";


@interface SMFlickrData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SMFlickrData

@synthesize stat = _stat;
@synthesize photos = _photos;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.stat = [self objectOrNilForKey:kSMFlickrDataStat fromDictionary:dict];
            self.photos = [SMPhotos modelObjectWithDictionary:[dict objectForKey:kSMFlickrDataPhotos]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.stat forKey:kSMFlickrDataStat];
    [mutableDict setValue:[self.photos dictionaryRepresentation] forKey:kSMFlickrDataPhotos];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.stat = [aDecoder decodeObjectForKey:kSMFlickrDataStat];
    self.photos = [aDecoder decodeObjectForKey:kSMFlickrDataPhotos];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_stat forKey:kSMFlickrDataStat];
    [aCoder encodeObject:_photos forKey:kSMFlickrDataPhotos];
}

- (id)copyWithZone:(NSZone *)zone
{
    SMFlickrData *copy = [[SMFlickrData alloc] init];
    
    if (copy) {

        copy.stat = [self.stat copyWithZone:zone];
        copy.photos = [self.photos copyWithZone:zone];
    }
    
    return copy;
}


@end
