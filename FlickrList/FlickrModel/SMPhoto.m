//
//  SMPhoto.m
//
//  Created by   on 2018. 11. 28.
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "SMPhoto.h"


NSString *const kSMPhotoSecret = @"secret";
NSString *const kSMPhotoId = @"id";
NSString *const kSMPhotoIsfamily = @"isfamily";
NSString *const kSMPhotoIspublic = @"ispublic";
NSString *const kSMPhotoFarm = @"farm";
NSString *const kSMPhotoOwner = @"owner";
NSString *const kSMPhotoServer = @"server";
NSString *const kSMPhotoUrlM = @"url_m";
NSString *const kSMPhotoTitle = @"title";
NSString *const kSMPhotoIsfriend = @"isfriend";
NSString *const kSMPhotoHeightM = @"height_m";
NSString *const kSMPhotoWidthM = @"width_m";


@interface SMPhoto ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SMPhoto

@synthesize secret = _secret;
@synthesize photoIdentifier = _photoIdentifier;
@synthesize isfamily = _isfamily;
@synthesize ispublic = _ispublic;
@synthesize farm = _farm;
@synthesize owner = _owner;
@synthesize server = _server;
@synthesize urlM = _urlM;
@synthesize title = _title;
@synthesize isfriend = _isfriend;
@synthesize heightM = _heightM;
@synthesize widthM = _widthM;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.secret = [self objectOrNilForKey:kSMPhotoSecret fromDictionary:dict];
            self.photoIdentifier = [self objectOrNilForKey:kSMPhotoId fromDictionary:dict];
            self.isfamily = [[self objectOrNilForKey:kSMPhotoIsfamily fromDictionary:dict] doubleValue];
            self.ispublic = [[self objectOrNilForKey:kSMPhotoIspublic fromDictionary:dict] doubleValue];
            self.farm = [[self objectOrNilForKey:kSMPhotoFarm fromDictionary:dict] doubleValue];
            self.owner = [self objectOrNilForKey:kSMPhotoOwner fromDictionary:dict];
            self.server = [self objectOrNilForKey:kSMPhotoServer fromDictionary:dict];
            self.urlM = [self objectOrNilForKey:kSMPhotoUrlM fromDictionary:dict];
            self.title = [self objectOrNilForKey:kSMPhotoTitle fromDictionary:dict];
            self.isfriend = [[self objectOrNilForKey:kSMPhotoIsfriend fromDictionary:dict] doubleValue];
            self.heightM = [self objectOrNilForKey:kSMPhotoHeightM fromDictionary:dict];
            self.widthM = [self objectOrNilForKey:kSMPhotoWidthM fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.secret forKey:kSMPhotoSecret];
    [mutableDict setValue:self.photoIdentifier forKey:kSMPhotoId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isfamily] forKey:kSMPhotoIsfamily];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ispublic] forKey:kSMPhotoIspublic];
    [mutableDict setValue:[NSNumber numberWithDouble:self.farm] forKey:kSMPhotoFarm];
    [mutableDict setValue:self.owner forKey:kSMPhotoOwner];
    [mutableDict setValue:self.server forKey:kSMPhotoServer];
    [mutableDict setValue:self.urlM forKey:kSMPhotoUrlM];
    [mutableDict setValue:self.title forKey:kSMPhotoTitle];
    [mutableDict setValue:[NSNumber numberWithDouble:self.isfriend] forKey:kSMPhotoIsfriend];
    [mutableDict setValue:self.heightM forKey:kSMPhotoHeightM];
    [mutableDict setValue:self.widthM forKey:kSMPhotoWidthM];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.secret = [aDecoder decodeObjectForKey:kSMPhotoSecret];
    self.photoIdentifier = [aDecoder decodeObjectForKey:kSMPhotoId];
    self.isfamily = [aDecoder decodeDoubleForKey:kSMPhotoIsfamily];
    self.ispublic = [aDecoder decodeDoubleForKey:kSMPhotoIspublic];
    self.farm = [aDecoder decodeDoubleForKey:kSMPhotoFarm];
    self.owner = [aDecoder decodeObjectForKey:kSMPhotoOwner];
    self.server = [aDecoder decodeObjectForKey:kSMPhotoServer];
    self.urlM = [aDecoder decodeObjectForKey:kSMPhotoUrlM];
    self.title = [aDecoder decodeObjectForKey:kSMPhotoTitle];
    self.isfriend = [aDecoder decodeDoubleForKey:kSMPhotoIsfriend];
    self.heightM = [aDecoder decodeObjectForKey:kSMPhotoHeightM];
    self.widthM = [aDecoder decodeObjectForKey:kSMPhotoWidthM];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_secret forKey:kSMPhotoSecret];
    [aCoder encodeObject:_photoIdentifier forKey:kSMPhotoId];
    [aCoder encodeDouble:_isfamily forKey:kSMPhotoIsfamily];
    [aCoder encodeDouble:_ispublic forKey:kSMPhotoIspublic];
    [aCoder encodeDouble:_farm forKey:kSMPhotoFarm];
    [aCoder encodeObject:_owner forKey:kSMPhotoOwner];
    [aCoder encodeObject:_server forKey:kSMPhotoServer];
    [aCoder encodeObject:_urlM forKey:kSMPhotoUrlM];
    [aCoder encodeObject:_title forKey:kSMPhotoTitle];
    [aCoder encodeDouble:_isfriend forKey:kSMPhotoIsfriend];
    [aCoder encodeObject:_heightM forKey:kSMPhotoHeightM];
    [aCoder encodeObject:_widthM forKey:kSMPhotoWidthM];
}

- (id)copyWithZone:(NSZone *)zone
{
    SMPhoto *copy = [[SMPhoto alloc] init];
    
    if (copy) {

        copy.secret = [self.secret copyWithZone:zone];
        copy.photoIdentifier = [self.photoIdentifier copyWithZone:zone];
        copy.isfamily = self.isfamily;
        copy.ispublic = self.ispublic;
        copy.farm = self.farm;
        copy.owner = [self.owner copyWithZone:zone];
        copy.server = [self.server copyWithZone:zone];
        copy.urlM = [self.urlM copyWithZone:zone];
        copy.title = [self.title copyWithZone:zone];
        copy.isfriend = self.isfriend;
        copy.heightM = [self.heightM copyWithZone:zone];
        copy.widthM = [self.widthM copyWithZone:zone];
    }
    
    return copy;
}


@end
