//
//  SMPhotos.m
//
//  Created by   on 2018. 11. 28.
//  Copyright (c) 2018 __MyCompanyName__. All rights reserved.
//

#import "SMPhotos.h"
#import "SMPhoto.h"


NSString *const kSMPhotosPhoto = @"photo";
NSString *const kSMPhotosPages = @"pages";
NSString *const kSMPhotosPerpage = @"perpage";
NSString *const kSMPhotosTotal = @"total";
NSString *const kSMPhotosPage = @"page";


@interface SMPhotos ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SMPhotos

@synthesize photo = _photo;
@synthesize pages = _pages;
@synthesize perpage = _perpage;
@synthesize total = _total;
@synthesize page = _page;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
    NSObject *receivedSMPhoto = [dict objectForKey:kSMPhotosPhoto];
    NSMutableArray *parsedSMPhoto = [NSMutableArray array];
    if ([receivedSMPhoto isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedSMPhoto) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedSMPhoto addObject:[SMPhoto modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedSMPhoto isKindOfClass:[NSDictionary class]]) {
       [parsedSMPhoto addObject:[SMPhoto modelObjectWithDictionary:(NSDictionary *)receivedSMPhoto]];
    }

    self.photo = [NSArray arrayWithArray:parsedSMPhoto];
            self.pages = [[self objectOrNilForKey:kSMPhotosPages fromDictionary:dict] doubleValue];
            self.perpage = [[self objectOrNilForKey:kSMPhotosPerpage fromDictionary:dict] doubleValue];
            self.total = [[self objectOrNilForKey:kSMPhotosTotal fromDictionary:dict] doubleValue];
            self.page = [[self objectOrNilForKey:kSMPhotosPage fromDictionary:dict] doubleValue];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    NSMutableArray *tempArrayForPhoto = [NSMutableArray array];
    for (NSObject *subArrayObject in self.photo) {
        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPhoto addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPhoto addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPhoto] forKey:kSMPhotosPhoto];
    [mutableDict setValue:[NSNumber numberWithDouble:self.pages] forKey:kSMPhotosPages];
    [mutableDict setValue:[NSNumber numberWithDouble:self.perpage] forKey:kSMPhotosPerpage];
    [mutableDict setValue:[NSNumber numberWithDouble:self.total] forKey:kSMPhotosTotal];
    [mutableDict setValue:[NSNumber numberWithDouble:self.page] forKey:kSMPhotosPage];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.photo = [aDecoder decodeObjectForKey:kSMPhotosPhoto];
    self.pages = [aDecoder decodeDoubleForKey:kSMPhotosPages];
    self.perpage = [aDecoder decodeDoubleForKey:kSMPhotosPerpage];
    self.total = [aDecoder decodeDoubleForKey:kSMPhotosTotal];
    self.page = [aDecoder decodeDoubleForKey:kSMPhotosPage];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_photo forKey:kSMPhotosPhoto];
    [aCoder encodeDouble:_pages forKey:kSMPhotosPages];
    [aCoder encodeDouble:_perpage forKey:kSMPhotosPerpage];
    [aCoder encodeDouble:_total forKey:kSMPhotosTotal];
    [aCoder encodeDouble:_page forKey:kSMPhotosPage];
}

- (id)copyWithZone:(NSZone *)zone
{
    SMPhotos *copy = [[SMPhotos alloc] init];
    
    if (copy) {

        copy.photo = [self.photo copyWithZone:zone];
        copy.pages = self.pages;
        copy.perpage = self.perpage;
        copy.total = self.total;
        copy.page = self.page;
    }
    
    return copy;
}


@end
