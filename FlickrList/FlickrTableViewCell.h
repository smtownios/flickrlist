//
//  FlickrTableViewCell.h
//  FlickrList
//
//  Created by JY on 2018. 11. 21..
//  Copyright © 2018년 JY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)setDataToCell:(NSString*)title :(NSString*)url;

@end
