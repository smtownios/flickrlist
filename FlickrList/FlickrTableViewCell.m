//
//  FlickrTableViewCell.m
//  FlickrList
//
//  Created by JY on 2018. 11. 21..
//  Copyright © 2018년 JY. All rights reserved.
//

#import "FlickrTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation FlickrTableViewCell

- (void)setDataToCell:(NSString*)title :(NSString*)url {
    self.titleLabel.text = title;
    [self.thumbnailImageView sd_setImageWithURL:[NSURL URLWithString: url] placeholderImage: nil];
    self.thumbnailImageView.clipsToBounds = true;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
