//
//  ViewController.h
//  FlickrList
//
//  Created by JY on 2018. 11. 21..
//  Copyright © 2018년 JY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end

