//
//  ViewController.m
//  FlickrList
//
//  Created by JY on 2018. 11. 21..
//  Copyright © 2018년 JY. All rights reserved.
//

#import "MainViewController.h"
#import "FlickrTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MainViewController.h"
#import "AFNetworking.h"
#import "FlickrData.h"
#import "SplashView.h"
#import "SMFlickrData.h"
#import "SMPhotos.h"
#import "SMPhoto.h"

@interface MainViewController ()

#warning Fix 5. Should object be set strong.
@property (strong, nonatomic) UIView *splashView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

#warning Fix 2. Should set Property's attributes.
@property (strong, nonatomic) NSMutableArray<SMPhoto *> *dataList;

@property (strong, nonatomic) UIActivityIndicatorView *indicatorView;

/* ---------- assign, weak, strong test ----------- */
@property (weak, nonatomic) NSData *data1;
@property (strong, nonatomic) NSData *data2;
@property (weak, nonatomic) NSData *someData;
@property (assign ,nonatomic) NSString *str1;
@property (strong ,nonatomic) NSString *str2;
/* ------------------------------------------------ */

#warning Fix 4. Use NSInteger instead of int.
@property (assign, nonatomic) NSInteger pageCount;

@property bool isLoading; //로딩중인지
@property NSString *URLString;

#warning Fix 1. Don't need to declare.

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    
    /* ---------- assign, weak, strong test ----------- */
    NSString* str = @"teststring";
    NSData* data = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSData* data1 = [str dataUsingEncoding:NSUTF8StringEncoding]; //1
    NSData* data2 = [str dataUsingEncoding:NSUTF8StringEncoding]; //1
    self.someData = data;
    NSLog(@"self.someData 11111 : %@",self.someData);

    self.str1 = @"weak str!";
    self.str2 = @"string str!";
    
    self.data1 = data1; // 1
    self.data2 = data2; // 2
    
    NSLog(@"======1======= data1 : %@, data2 : %@",self.data1, self.data2);
    
    NSLog(@"weak str : %@, strong str : %@",self.str1, self.str2);
    /* ------------------------------------------------ */
    
    
    self.pageCount = 1;
    self.isLoading = false;
    self.URLString = @"https://api.flickr.com/services/rest?method=flickr.photos.getRecent&api_key=a7b7b6ba8cc4213db5f1a773830e07a5&format=json&extras=url_m&nojsoncallback=1&page=";
    
    self.dataList = [[NSMutableArray alloc] init];
    
    // Splash View
    [self showSplashView];
    
    // load data
    [self loadData:self.pageCount];
}

- (void)showSplashView {
    self.splashView =[[[NSBundle mainBundle] loadNibNamed:@"SplashView" owner:self options:nil] firstObject];
    [self.splashView setFrame:self.view.frame];
    
    UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
    [currentWindow addSubview:self.splashView];
    
}

- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"self.someData 22222 : %@",self.someData);
    NSLog(@"self.data2 : %@",self.data2);
    NSLog(@"self.data1 : %@",self.data1);
}

- (void)removeSplashView {
    [self.splashView removeFromSuperview];
    self.splashView = nil;
    NSLog(@"self.someData 22222 : %@",self.someData.description);
}

- (void)toggleIndicatorView:(BOOL)value {
    if(value){
        // show indicator view
        self.indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
        [self.indicatorView setCenter:self.view.center];
        [self.indicatorView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self.indicatorView setBackgroundColor:UIColor.darkGrayColor];
        
        [self.view addSubview:self.indicatorView];
        [self.indicatorView startAnimating];
        
        [self.view setUserInteractionEnabled:false];
    } else {
        // hide indicator view
        [self.indicatorView stopAnimating];
        [self.indicatorView removeFromSuperview];
        
        [self.view setUserInteractionEnabled:true];
    }
}

- (void)loadData:(NSInteger)count {
    self.isLoading = true;
    [self toggleIndicatorView:true];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *newString = [self.URLString stringByAppendingFormat:@"%zd", self.pageCount];
    [manager GET:newString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        // JSON Accelerator 사용
        SMFlickrData *jsonObject = [SMFlickrData modelObjectWithDictionary:responseObject];
        [self.dataList addObjectsFromArray:jsonObject.photos.photo];
        
        [self setUI];
        self.isLoading = false;
        
        NSLog(@"self.someData 22222 : %@",self.someData);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void)setUI {
#warning Fix 3. Must be UI updating on the main thread.
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        // Remove Splash View
        if(self.splashView){
            [self removeSplashView];
        }
        [self toggleIndicatorView:false];
        NSLog(@"weak str : %@, strong str : %@",self.str1, self.str2);
    });
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FlickrTableViewCell *cell = [ self.tableView dequeueReusableCellWithIdentifier: @"flickrCell" forIndexPath: indexPath];
    
    SMPhoto *photo = self.dataList[indexPath.row];
    [cell setDataToCell:photo.title :photo.urlM];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // 더 불러오기
    if(indexPath.row == [self.dataList count]-1) {
        if(!self.isLoading){
            self.pageCount ++;
            [self loadData:self.pageCount];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

@end
